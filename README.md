# ALL cell recognition

This project was part of an introductory course in Deep learning at Umeå University. 

The aim of the project was to utlize the model EfficientNet V2 B3 in a transfer learning case where a custom classifier was used to determine if a blood cell presents with acute lymphiblastic leukemia (ALL) or not. It uses a dataset published in 2019 containing about 17000 images classified by an oncologist into the classes healty or calcerous cells.

## Sources

The following sources are responsible for the data.

[1]	S. Mourya, S. Kant, P. Kumar, A. Gupta, and R. Gupta, “ALL Challenge dataset of ISBI 2019.” The Cancer Imaging Archive, 2019. doi: 10.7937/TCIA.2019.DC64I46R.
[2]	A. Gupta et al., “GCTI-SN: Geometry-inspired chemical and tissue invariant stain normalization of microscopic medical images,” Med. Image Anal., vol. 65, p. 101788, Oct. 2020, doi: 10.1016/j.media.2020.101788.
[3]	R. Duggal, A. Gupta, R. Gupta, and P. Mallick, “SD-Layer: Stain Deconvolutional Layer for CNNs in Medical Microscopic Imaging,” in Medical Image Computing and Computer Assisted Intervention − MICCAI 2017, vol. 10435, M. Descoteaux, L. Maier-Hein, A. Franz, P. Jannin, D. L. Collins, and S. Duchesne, Eds. Cham: Springer International Publishing, 2017, pp. 435–443. doi: 10.1007/978-3-319-66179-7_50.
[4]	S. Gehlot, A. Gupta, and R. Gupta, “SDCT-AuxNet : DCT augmented stain deconvolutional CNN with auxiliary classifier for cancer diagnosis,” Med. Image Anal., vol. 61, p. 101661, Apr. 2020, doi: 10.1016/j.media.2020.101661.